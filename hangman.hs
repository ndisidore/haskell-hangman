module Hangman where

----------------------------------------------------------------------
-- imports
import System.IO
import Data.List
import Char
import Debug.Trace

----------------------------------------------------------------------
-- type/data defintions

type GuessList = [Char]
type Word = [Letter]
type WordStatus = (Word, GuessList, NumGuesses)
type NumGuesses = Int

-- state of each guess
data Guess 	= Valid Char
			| Repeated
			| Invalid
			deriving (Eq)

-- status of each letter in word
data Letter	= Guessed Char
			| Hidden Char
			deriving (Eq,Ord)

-- game status
data GameStatus	= Win 
			| InGame WordStatus
			| Lose

-- want to show blanks where letters haven't been guessed
instance Show Letter where
	show (Guessed x) = show x
	show (Hidden x) = "_"

----------------------------------------------------------------------
-- constants
maxGuesses = 6


----------------------------------------------------------------------
-- functions
main :: IO ()
main = runHangman

runHangman :: IO ()
runHangman = do
	--run guesser first
	--word <- hGetLine h
	--test word "first"
	outcome <- doGuess $ firstState "first"
	case outcome of
		Win -> putStrLn $ "You Win!"
		Lose -> putStrLn $ "You Lose :("
	return ()

-- construct the first WordStatus state
firstState :: String -> WordStatus
firstState w = let letters = [Hidden (toLower x) | x <- w, isAlpha x]
	       in (letters, [], 0)

-- the letter guess loop
doGuess :: WordStatus -> IO GameStatus
doGuess st@(w, g, n) = do
	putStrLn $ "Current word status: " ++ show w
	putStrLn $ "Guesses: " ++ show g
	putStrLn $ "# Incorrect: " ++ show n
	putStr $ "Enter Guess: "
	c <- getChar	--get the character
	if c == '\n' then doGuess (w,g,n)
	   else
		let (w', g', n') = takeGuess st c
		    stat = checkStatus (w', g', n')
		    in case stat of
			Lose -> return Lose
			Win -> return Win
			InGame _ -> doGuess (w',g',n')
	
-- Status of current game
checkStatus :: WordStatus -> GameStatus
checkStatus st@(w, g, n)
	| and (map allGuessed w) = Win
	| n >= maxGuesses = Lose
	| otherwise = InGame st
	
-- help construct true/false list for each letter
allGuessed :: Letter -> Bool
allGuessed (Guessed l) = True
allGuessed (Hidden l) = False

-- takes status and guess and handles as needed
takeGuess :: WordStatus -> Char -> WordStatus
takeGuess st@(word, guesses, num) x = do
	let smallX = toLower x
	    in case checkGuess smallX guesses of
		Invalid -> error "Please enter a valid character"
		Repeated -> error "Letter has already been guessed"
		Valid ch -> validGuess st ch

-- check to make sure guess even makes sense/is repeat
checkGuess :: Char -> GuessList -> Guess
checkGuess x y
	| isAlpha x = if x `elem` y then Repeated
			else Valid x
	| otherwise = Invalid

-- for valid guesses, add guess to guesslist and make needed changes to word
validGuess :: WordStatus -> Char -> WordStatus
validGuess (w, g, n) x =
	--construct a new Word with corresponding changes
	let w' = map (switchLetter x) w
            g' = g ++ [x]
	    n' = if any (checkHit x) w then n else n+1
	in (w', g', n')

-- for each letter in word, if letter == entered char then the letter was a hit
checkHit :: Char -> Letter -> Bool
checkHit x (Guessed l) = False
checkHit x (Hidden l) = if l == x then True else False

-- for each letter in word, if letter == entered char then change status
switchLetter :: Char -> Letter -> Letter
switchLetter x (Guessed l) = Guessed l
switchLetter x (Hidden l) = if l == x then Guessed x else Hidden l
