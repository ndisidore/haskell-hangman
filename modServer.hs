module NetInterface where

-- runhaskell modServer.hs
-- telnet localhost 3468
import Network
import System.IO
import Control.Concurrent

comPort = 3468
type Msg = String

main :: IO ()
main = withSocketsDo $ do
	--set up server socket listening on port
	sock <- listenOn $ PortNumber comPort
	--create a communication channel
	chan <- newChan
	putStrLn $ "Listening on " ++ (show comPort)
	newCon sock chan

newCon :: Socket -> Chan Msg -> IO ()
newCon sock chan = do
	--accept incoming connection on the socket
	(handle, host, port) <- accept sock
	putStrLn $ "Connected to "++ show host ++ " on port " ++ show port
	--set nobuffering so we don't need to flush before content written
	hSetBuffering handle NoBuffering
	pInfo <- hShow handle	-- general connection info
	putStrLn $ "Port info: " ++ pInfo
	--fork a new thread to handle each connection
	forkIO $ conHandler handle chan
	newCon sock chan

conHandler :: Handle -> Chan Msg -> IO ()
conHandler handle chan = do
	--duplicate the channel so the thread has a "write"-end to the channel
	chan' <- dupChan chan
	--fork a reciever thread for current connection
	read <- forkIO $ receive handle chan'
	send handle chan
	--once sender method exits, kill the connection
	killThread read
	hClose handle

receive :: Handle -> Chan Msg -> IO ()
receive h ch = do
	--get line from channel
	cur <- readChan ch
	hPutStrLn h $ "Recieved: " ++ (show cur)
	receive h ch

send :: Handle -> Chan Msg -> IO ()
send h ch = do
	hPutStr h "Send (q to close): "
	input <- hGetLine h
	--do case on input to check if quit
	--need to strip off trailing return (\r)
        case init input of
		"q" -> hPutStrLn h "Bye!"
		_   -> do
			writeChan ch $ init input
			send h ch
        
